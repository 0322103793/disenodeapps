import React, { useEffect, useState } from 'react';
import { Button, ScrollView, StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const App = () => {
  const [pendientes, setPendientes] = useState([]);

  useEffect(() => {
    obtenerPendientes();
  }, []);

  const obtenerPendientes = async () => {
    try {
      const response = await fetch('http://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      setPendientes(data);
    } catch (error) {
      console.error('Error al obtener los pendientes:', error);
    }
  };

  return (
    <View style={styles.appContainer}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Menu">
          <Stack.Screen name="NFL" component={MenuPendientes} />
          <Stack.Screen name="PendientesIDs" component={PendientesIDs} initialParams={{ pendientes }} />
          <Stack.Screen name="PendientesIDsTitulos" component={PendientesIDsTitulos} initialParams={{ pendientes }} />
          <Stack.Screen name="PendientesSinResolver" component={PendientesSinResolver} initialParams={{ pendientes }} />
          <Stack.Screen name="PendientesResueltos" component={PendientesResueltos} initialParams={{ pendientes }} />
          <Stack.Screen name="PendientesIDsUsuarios" component={PendientesIDsUsuarios} initialParams={{ pendientes }} />
          <Stack.Screen name="PendientesResueltosUsuarios" component={PendientesResueltosUsuarios} initialParams={{ pendientes }} />
          <Stack.Screen name="PendientesSinResolverUsuarios" component={PendientesSinResolverUsuarios} initialParams={{ pendientes }} />
        </Stack.Navigator>
      </NavigationContainer>
    </View>
  );
};

const MenuPendientes = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.description}>
          Esta es una aplicación de la NFL que muestra diferentes listas de pendientes relacionados con los partidos, jugadores y estadísticas. Por favor, selecciona una opción del menú:
        </Text>
        <View style={styles.headingContainer}>
          <Text style={styles.heading}>Menú de Pendientes</Text>
        </View>
        <View style={styles.menuContainer}>
          <View style={styles.buttonsContainer}>
            <Button title="Lista de Pendientes (Solo IDs)" onPress={() => navigation.navigate('PendientesIDs')} color="#808080" />
            <Button title="Lista de Pendientes (IDs y Títulos)" onPress={() => navigation.navigate('PendientesIDsTitulos')} color="#808080" />
            <Button title="Pendientes Sin Resolver (ID y Título)" onPress={() => navigation.navigate('PendientesSinResolver')} color="#808080" />
            <Button title="Pendientes Resueltos (ID y Título)" onPress={() => navigation.navigate('PendientesResueltos')} color="#808080" />
            <Button title="Lista de Pendientes (IDs y Usuario ID)" onPress={() => navigation.navigate('PendientesIDsUsuarios')} color="#808080" />
            <Button title="Pendientes Resueltos (ID y Usuario ID)" onPress={() => navigation.navigate('PendientesResueltosUsuarios')} color="#808080" />
            <Button title="Pendientes Sin Resolver (ID y Usuario ID)" onPress={() => navigation.navigate('PendientesSinResolverUsuarios')} color="#808080" />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const PendientesIDs = ({ route }) => {
  const { pendientes } = route.params;
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.heading}>Lista de Pendientes (Solo IDs)</Text>
        {pendientes.map(pendiente => (
          <Text key={pendiente.id}>{pendiente.id}</Text>
        ))}
      </ScrollView>
    </View>
  );
};

const PendientesIDsTitulos = ({ route }) => {
  const { pendientes } = route.params;
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.heading}>Lista de Pendientes (IDs y Títulos)</Text>
        {pendientes.map(pendiente => (
          <Text key={pendiente.id}>{pendiente.id} - {pendiente.title}</Text>
        ))}
      </ScrollView>
    </View>
  );
};

const PendientesSinResolver = ({ route }) => {
  const { pendientes } = route.params;
  const pendientesSinResolver = pendientes.filter(pendiente => !pendiente.completed);
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.heading}>Pendientes Sin Resolver (ID y Título)</Text>
        {pendientesSinResolver.map(pendiente => (
          <Text key={pendiente.id}>{pendiente.id} - {pendiente.title}</Text>
        ))}
      </ScrollView>
    </View>
  );
};

const PendientesResueltos = ({ route }) => {
  const { pendientes } = route.params;
  const pendientesResueltos = pendientes.filter(pendiente => pendiente.completed);
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.heading}>Pendientes Resueltos (ID y Título)</Text>
        {pendientesResueltos.map(pendiente => (
          <Text key={pendiente.id}>{pendiente.id} - {pendiente.title}</Text>
        ))}
      </ScrollView>
    </View>
  );
};

const PendientesIDsUsuarios = ({ route }) => {
  const { pendientes } = route.params;
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.heading}>Lista de Pendientes (IDs y Usuario ID)</Text>
        {pendientes.map(pendiente => (
          <Text key={pendiente.id}>{pendiente.id} - {pendiente.userId}</Text>
        ))}
      </ScrollView>
    </View>
  );
};

const PendientesResueltosUsuarios = ({ route }) => {
  const { pendientes } = route.params;
  const pendientesResueltos = pendientes.filter(pendiente => pendiente.completed);
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.heading}>Pendientes Resueltos (ID y Usuario ID)</Text>
        {pendientesResueltos.map(pendiente => (
          <Text key={pendiente.id}>{pendiente.id} - {pendiente.userId}</Text>
        ))}
      </ScrollView>
    </View>
  );
};

const PendientesSinResolverUsuarios = ({ route }) => {
  const { pendientes } = route.params;
  const pendientesSinResolver = pendientes.filter(pendiente => !pendiente.completed);
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.heading}>Pendientes Sin Resolver (ID y Usuario ID)</Text>
        {pendientesSinResolver.map(pendiente => (
          <Text key={pendiente.id}>{pendiente.id} - {pendiente.userId}</Text>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  appContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  scrollContainer: {
    alignItems: 'justify',
  },
  description: {
    textAlign: 'justify',
    marginBottom: 20,
    fontSize: 16,
    lineHeight: 24,
  },
  headingContainer: {
    marginBottom: 20,
    alignItems: 'center',
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  menuContainer: {
    width: '100%',
  },
  buttonsContainer: {
    justifyContent: 'space-between',
    width: '100%',
  },
});

export default App;
