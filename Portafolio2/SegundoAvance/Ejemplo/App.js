// Importa React y el componente NavigationContainer de react-navigation
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

// Importa el componente Navigation desde el archivo Navigation.js
import Navigation from './Navigation';

// Componente principal de la aplicación
export default function App() {
  return (
    // Contenedor de navegación que envuelve toda la aplicación
    <NavigationContainer>
      {/* Renderiza el componente de navegación */}
      <Navigation />
    </NavigationContainer>
  );
}

