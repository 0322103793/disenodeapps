// Importa las funciones necesarias de React Navigation
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

// Importa los componentes de las distintas pantallas
import HomeScreen from "./screens/HomeScreen";
import SettingsScreen from "./screens/SettingsScreen";
import Profile from "./screens/Profile";
import StackScreen from "./screens/StackScreen"; 

// Crea un TabNavigator para las pestañas inferiores
const Tab = createBottomTabNavigator();
// Crea un StackNavigator para la navegación entre pantallas
const Stack = createStackNavigator();

// Función para renderizar el StackNavigator
function MyStack() {
  return (
    <Stack.Navigator>
      {/* Define la pantalla StackScreen dentro del StackNavigator */}
      <Stack.Screen name="Stack" component={StackScreen} />
    </Stack.Navigator>
  );
}

// Función para renderizar el TabNavigator
function MyTabs() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        // Define opciones para el estilo de la barra de pestañas
        activeTintColor: "#007AFF", // Color del ícono activo
        inactiveTintColor: "#8E8E93", // Color del ícono inactivo
        labelStyle: {
          fontSize: 16, // Tamaño de la fuente de las etiquetas
          fontWeight: "bold" // Peso de la fuente de las etiquetas
        }
      }}
    >
      {/* Define las pantallas de las pestañas */}
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Stack" component={MyStack} />
      <Tab.Screen name="Settings" component={SettingsScreen} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
}

// Componente principal de navegación que contiene la navegación
export default function Navigation() {
  return (
    // Contenedor de navegación que envuelve el TabNavigator
    <NavigationContainer>
      <MyTabs /> {/* Renderiza el TabNavigator */}
    </NavigationContainer>
  );
}

