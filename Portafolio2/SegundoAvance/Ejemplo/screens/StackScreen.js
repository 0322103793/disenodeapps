// Importa React y los componentes necesarios de React Native
import React from "react";
import { View, Text, StyleSheet } from 'react-native';

// Componente para la pantalla del StackNavigator
export default function StackScreen() {
  return (
    // Vista principal de la pantalla
    <View style={styles.container}>
      {/* Texto que indica que es la pantalla del StackNavigator */}
      <Text style={styles.text}>Stack Screen</Text>
    </View>
  );
}

// Estilos para la pantalla del StackNavigator
const styles = StyleSheet.create({
  container: {
    flex: 1, // Ocupa todo el espacio disponible
    justifyContent: 'center', // Alineación vertical centrada
    alignItems: 'center', // Alineación horizontal centrada
    backgroundColor: '#FFFFFF' // Fondo blanco
  },
  text: {
    fontSize: 30, // Tamaño de la fuente
    fontWeight: 'bold', // Fuente en negrita
    color: '#007AFF' // Color de texto azul
  }
});

