// Importaciones de React y componentes de React Native
import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import * as ImagePicker from 'expo-image-picker';

// Importaciones de componentes personalizados
import Button from './components/Button';
import ImageViewer from './components/imageViewer';
import CircleButton from './components/CircleButton';
import IconButton from './components/IconButton';
import EmojiPicker from "./components/EmojiPicker";

// Importación de la imagen de relleno
const PlaceholderImage = require('./assets/adaptive-icon.png');

// Función principal del componente App
export default function App() {
  // Estados para gestionar la visibilidad del modal, opciones de la aplicación y la imagen seleccionada
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [showAppOptions, setShowAppOptions] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);

  // Función para seleccionar una imagen desde la galería
  const pickImageAsync = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 1,
    });

    if (!result.canceled) {
      setSelectedImage(result.assets[0].uri);
      setShowAppOptions(true);
    } else {
      alert('You did not select any image.');
    }
  };

  // Función para restablecer las opciones de la aplicación
  const onReset = () => {
    setShowAppOptions(false);
  };

  // Función para abrir el modal de selección de sticker
  const onAddSticker = () => {
    setIsModalVisible(true);
  };

  // Función para guardar la imagen
  const onSaveImageAsync = async () => {
    // Implementa la funcionalidad de guardar la imagen aquí
  };

  // Función para cerrar el modal
  const onModalClose = () => {
    setIsModalVisible(false);
  };

  // Renderización del componente
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        // Componente para visualizar la imagen
        <ImageViewer
          placeholderImageSource={PlaceholderImage}
          selectedImage={selectedImage}/>
      </View>
      // Renderiza las opciones de la aplicación o el botón de selección de foto según el estado
      {showAppOptions ? (
        <View style={styles.optionsContainer}>
          <View style={styles.optionsRow}>
            // Botones de opciones
            <IconButton icon="refresh" label="Reset" onPress={onReset} />
            <CircleButton onPress={onAddSticker} />
            <IconButton icon="save-alt" label="Save" onPress={onSaveImageAsync} />
          </View>
        </View>
      ) : (
        <View style={styles.footerContainer}>
          // Botones para seleccionar una foto
          <Button theme="primary" label="Choose a photo" onPress={pickImageAsync} />
          <Button label="Use this photo" onPress={() => setShowAppOptions(true)} />
        </View>
      )}
      // Modal de selección de emoji
      <EmojiPicker isVisible={isModalVisible} onClose={onModalClose}>
        // Contenido del modal
        {}
      </EmojiPicker>
      <StatusBar style="auto" />
    </View>
  );
      }  
// Estilos del componente
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imageContainer: {
    flex: 1,
    paddingTop: 58,
    alignItems: 'center',
    justifyContent: 'center',
  },
  footerContainer: {
    marginBottom: 20,
    alignItems: 'center',
  },
  optionsContainer: {
    position: 'absolute',
    bottom: 80,
    alignSelf: 'center',
  },
  optionsRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '70%',
  },
});
