import { Modal, View, Text, Pressable, StyleSheet } from 'react-native'; // Importa componentes necesarios
import MaterialIcons from '@expo/vector-icons/MaterialIcons'; // Importa el ícono MaterialIcons

// Componente de selector de emoji
export default function EmojiPicker({ isVisible, children, onClose }) {
  return (
    // Modal para mostrar el selector de emoji
    <Modal animationType="slide" transparent={true} visible={isVisible}>
      {/* Contenido del modal */}
      <View style={styles.modalContent}>
        {/* Contenedor del título y el botón de cierre */}
        <View style={styles.titleContainer}>
          {/* Título del modal */}
          <Text style={styles.title}>Choose a sticker</Text>
          {/* Botón para cerrar el modal */}
          <Pressable onPress={onClose}>
            <MaterialIcons name="close" color="#fff" size={22} />
          </Pressable>
        </View>
        {/* Contenido adicional proporcionado por el componente hijo */}
        {children}
      </View>
    </Modal>
  );
}

// Estilos del componente
const styles = StyleSheet.create({
  modalContent: {
    height: '25%', // Altura del modal
    width: '100%', // Ancho del modal
    backgroundColor: '#25292e', // Color de fondo del modal
    borderTopRightRadius: 18, // Radio de la esquina superior derecha
    borderTopLeftRadius: 18, // Radio de la esquina superior izquierda
    position: 'absolute', // Posición absoluta
    bottom: 0, // Alinear en la parte inferior
  },
  titleContainer: {
    height: '16%', // Altura del contenedor del título
    backgroundColor: '#464C55', // Color de fondo del contenedor del título
    borderTopRightRadius: 10, // Radio de la esquina superior derecha del contenedor del título
    borderTopLeftRadius: 10, // Radio de la esquina superior izquierda del contenedor del título
    paddingHorizontal: 20, // Relleno horizontal
    flexDirection: 'row', // Diseño de fila
    alignItems: 'center', // Alineación de los elementos al centro verticalmente
    justifyContent: 'space-between', // Distribución de espacio entre los elementos
  },
  title: {
    color: '#fff', // Color del texto del título
    fontSize: 16, // Tamaño de fuente del título
  },
  pickerContainer: {
    flexDirection: 'row', // Diseño de fila
    justifyContent: 'center', // Alinear al centro horizontalmente
    alignItems: 'center', // Alinear al centro verticalmente
    paddingHorizontal: 50, // Relleno horizontal
    paddingVertical: 20, // Relleno vertical
  },
});
