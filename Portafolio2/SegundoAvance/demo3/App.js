import React from 'react';  // Importa React para poder definir componentes de React
import { StatusBar } from 'expo-status-bar';  // Importa el StatusBar de Expo para gestionar la barra de estado
import { StyleSheet, View } from 'react-native';  // Importa StyleSheet y View de React Native para gestionar los estilos y la estructura de la interfaz

// Importa los componentes personalizados para mostrar el valor de Bitcoin en diferentes monedas
import BtcUsd from './components/BtcUsd';
import BtcEuro from './components/BtcEuro';
import BtcGbp from './components/BtcGbp';

// Define el componente App como una función de React
export default function App() {
  return (
    <View style={styles.container}>  
      <View style={styles.box}>  
        <BtcUsd />  
      </View>
      <View style={styles.box}>  
        <BtcEuro />  
      </View>
      <View style={styles.box}>  
        <BtcGbp />  
      </View>
      <StatusBar style="auto" />  
    </View>
  );
}

// Define los estilos usando StyleSheet.create para mantener los estilos encapsulados y optimizados para React Native
const styles = StyleSheet.create({
  container: {  // Estilos para el contenedor principal
    flex: 1,  // Se expande para llenar todo el espacio disponible
    backgroundColor: 'gray',  // Fondo gris
    alignItems: 'center',  // Centrado horizontal de los elementos hijos
    justifyContent: 'center',  // Centrado vertical de los elementos hijos
  },
  box: {  // Estilos para los contenedores internos
    width: '90%',  // Ancho del 90% del contenedor principal
    backgroundColor: 'white',  // Fondo blanco
    borderRadius: 5,  // Bordes redondeados
    padding: 20,  // Relleno interior
    marginBottom: 40,  // Margen inferior
    shadowColor: '#000',  // Color de la sombra
    shadowOffset: {  // Desplazamiento de la sombra
      width: 0,  // Sin desplazamiento horizontal
      height: 2,  // Desplazamiento vertical de 2 unidades
    },
  },
});
