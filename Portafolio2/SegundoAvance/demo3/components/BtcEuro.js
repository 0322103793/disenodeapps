import React, { useState, useEffect } from 'react'; // Importa React y sus hooks useState y useEffect
import { Text, View, ActivityIndicator, StyleSheet } from 'react-native'; // Importa componentes de React Native para la interfaz

const BtcEuro = () => {
  const [response, setResponse] = useState(null); // Estado para almacenar la respuesta de la API
  const [isLoading, setIsLoading] = useState(true); // Estado para controlar el estado de carga
  const [error, setError] = useState(null); // Estado para manejar errores

  const url = "https://api.coindesk.com/v1/bpi/currentprice.json"; // URL de la API para obtener el precio actual de Bitcoin en euros

  useEffect(() => {
    // Efecto para cargar los datos de la API cuando el componente se monta
    fetch(url) // Realiza una solicitud GET a la URL
      .then(res => res.json()) // Convierte la respuesta en formato JSON
      .then((result) => {
        setIsLoading(false); // Cambia el estado de carga a falso
        setResponse(result); // Almacena la respuesta en el estado
      })
      .catch((error) => {
        setIsLoading(false); // Cambia el estado de carga a falso
        setError(error); // Almacena cualquier error en el estado
      });
  }, [url]); // Se ejecuta solo cuando la URL cambia

  // Función para determinar qué contenido renderizar en función del estado
  const getContent = () => {
    if (isLoading) {
      // Si está cargando, muestra un indicador de carga
      return (
        <View style={styles.container}>
          <Text style={styles.textSize}>Loading Data...</Text>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    if (error) {
      // Si hay un error, muestra un mensaje de error
      return (
        <View style={styles.container}>
          <Text>Error: {error.message}</Text>
        </View>
      );
    }
    // Si la carga ha terminado y no hay errores, muestra el precio de Bitcoin en euros
    return (
      <View style={styles.container}>
        <Text style={styles.textSize}>BTC to EURO: ${response && response.bpi && response.bpi.EUR.rate}</Text>
      </View>
    );
  };

  // Renderiza el contenido en función del estado
  return getContent();
};

// Estilos para el componente
const styles = StyleSheet.create({
  container: {
    marginVertical: 10, // Margen vertical
    alignItems: 'center', // Alinea los elementos al centro
  },
  textSize: {
    fontSize: 20 // Tamaño de fuente
  },
});

export default BtcEuro; // Exporta el componente BtcEuro
