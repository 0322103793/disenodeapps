// Importaciones necesarias de React y React Native
import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
// Importación del componente Pokedex
import Pokedex from './components/Pokedex';

// Función principal de la aplicación
export default function App() {
  return (
    // Contenedor principal de la aplicación
    <View style={styles.container}>
      {/* Renderiza el componente Pokedex */}
      <Pokedex />
      {/* Barra de estado */}
      <StatusBar style="auto" />
    </View>
  );
}

// Estilos para el contenedor principal
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
