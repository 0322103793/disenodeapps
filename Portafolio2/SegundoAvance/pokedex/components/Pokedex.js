import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator, TextInput } from 'react-native';

// URL de la API
const url = "https://pokeapi.deno.dev/pokemon";

export default function Pokedex() {
    // Estados para almacenar los datos, errores y estado de carga
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [searchQuery, setSearchQuery] = useState('');

    // Efecto de efectuar la solicitud a la API
    useEffect(() => {
        fetch(url)
            .then(response => response.json())
            .then((result) => {
                setIsLoading(false);
                setData(result);
            })
            .catch((error) => {
                setIsLoading(false);
                setError(error);
            });
    }, []);

    // Filtrar los datos basados en la búsqueda
    const filteredData = data ? data.filter(item => item.name.toLowerCase().includes(searchQuery.toLowerCase())) : [];

    // Función para renderizar el contenido
    const getContent = () => {
        // Si está cargando, muestra el indicador de carga
        if (isLoading) {
            return (
                <View style={styles.container}>
                    <Text style={styles.loadingText}>Loading Data...</Text>
                    <ActivityIndicator size="large" color="pink" />
                </View>
            );
        }
        // Si hay un error, muestra el mensaje de error
        if (error) {
            return <Text style={styles.errorText}>Error: {error.message}</Text>;
        }
        // Si no hay datos, muestra un mensaje de que no hay datos disponibles
        if (!data) {
            return <Text style={styles.errorText}>No data available</Text>;
        }
        // Si hay datos y no hay errores, muestra la lista de Pokémon filtrada
        return (
            <View style={styles.container}>
                <TextInput
                    style={styles.searchInput}
                    placeholder="Search Pokémon..."
                    onChangeText={(text) => setSearchQuery(text)}
                    value={searchQuery}
                />
                <FlatList
                    data={filteredData}
                    numColumns={3}
                    renderItem={({ item }) => (
                        <View style={styles.pokemonContainer}>
                            <Image style={styles.image} source={{ uri: item.imageUrl }} />
                            <Text style={styles.name}>Name: {item.name}</Text>
                            <Text style={styles.genus}>Genus: {item.genus}</Text>
                            <Text style={styles.types}>Types: {item.types.join(', ')}</Text>
                        </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    contentContainerStyle={styles.flatListContainer}
                />
            </View>
        );
    };

    return (
        <View>
            {getContent()}
        </View>
    );
}

// Estilos
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5',
    },
    loadingText: {
        fontSize: 24,
        marginBottom: 10,
    },
    errorText: {
        fontSize: 24,
        color: 'red',
    },
    searchInput: {
        width: '90%',
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 8,
    },
    pokemonContainer: {
        marginBottom: 20,
        alignItems: 'center',
        flex: 1,
        margin: 4,
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: 10,
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 5,
    },
    genus: {
        fontSize: 16,
        fontStyle: 'italic',
    },
    types: {
        fontSize: 16,
    },
    image: {
        width: 100,
        height: 100,
        resizeMode: 'contain',
    },
    flatListContainer: {
        paddingHorizontal: 8,
        paddingTop: 8,
    },
});
