const express = require('express');
const puestos = express.Router();

//--------------------------------------------------Ruta de prueba(consulta)

puestos.get('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('SELECT * FROM puestos', (err, rows) => {
            if (err) return res.send(err)

            res.json(rows)
        })
    })
});

//--------------------------------------------------Ruta de prueba(insertar)

puestos.post('/', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('INSERT INTO puestos SET ?', [req.body], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se agrego exitosamente!')
        })
    })
});

//--------------------------------------------------Ruta de prueba(Eliminar)

puestos.delete('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('DELETE FROM puestos WHERE numero = ?', [req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se elimino exitosamente!')
        })
    })
});


//--------------------------------------------------Ruta de prueba(Actualizar)

puestos.put('/:numero', (req, res) => {
    req.getConnection((err, connection) => {
        if (err) return res.send(err)

        connection.query('UPDATE puestos SET ? WHERE numero = ?', [req.body, req.params.numero], (err, rows) => {
            if (err) return res.send(err)

            res.send('Se actualizo exitosamente')
        })
    })
});

//---------------------------------------------------Exportar modulo
module.exports = puestos