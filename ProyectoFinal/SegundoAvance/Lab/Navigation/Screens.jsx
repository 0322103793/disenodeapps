import { useNavigation } from '@react-navigation/native';
export const Screens = () => {
    const navigation = useNavigation();
    
    const Empleados = () => {
      navigation.navigate('Empleados');
    };

    const Almacen = () => {
      navigation.navigate('Almacen');
    };

    const Main = () => {
      navigation.navigate('Main');
    };
  
    return {
      Empleados,
      Almacen,
      Main,
    };
};