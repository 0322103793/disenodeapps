import { VStack, View, ScrollView } from '@gluestack-ui/themed';
import React from 'react';
import { StyleSheet } from 'react-native';
import { HeadChart, GraphicCard, Title, Footer } from '../Components/Main';

const Almacen = () => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <VStack alignItems="stretch" alignContent="space-between">
          {/* Head */}
          <HeadChart />
          {/* EndHead */}
          <View>
            <VStack>
              {/* Title of Content */}
              <Title text="Almacen" />
              {/* EndTitle */}
              <View alignItems="center" >
                <VStack space="2xl">
                  {/* Content */}
                  <GraphicCard text="Piezas" color="#6D4B70" />
                  <GraphicCard text="Modelos" color="#47516C" />
                  <GraphicCard text="Materiales" color="#6D4B70" />
                  {/* EndContent */}
                </VStack>
              </View>
            </VStack>
            
          </View>
        </VStack>
        <Footer color='transparent'/>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EAEBF0',
  },
});

export default React.memo(Almacen);

