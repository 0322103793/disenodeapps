// Api del BackEnd
import  * as Token  from "../Data/Token";
import * as User from "../Data/User";
import { useEffect, useState } from "react";

export const URL = "http://172.18.6.138:8000/";


export const GET = (ruta) => {
    const  extension = ruta;
    const endpoint = `${URL}${extension}`;
    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchGET = async() => {
            try {
                const authToken = await Token.getToken();
                const response = await fetch(endpoint, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${authToken}`
                    }
                });
                const Data = await response.json();
                setData(Data);
            } catch {    
                setData(null);            
            }
        };
        // console.log(data)
        fetchGET();    
    }, []);
    

    return data;
};


export const usePOST = async (ruta, datas) => {
    const max = 35;
    const min = 5;
    const extension = ruta; 
    const endpoint = `${URL}${extension}`;
    const [data, setData] = useState([]);

    // useEffect(() => {
    //     const fetchPOST = async() => {
            try {
                const authToken = await Token.getToken();
                const response = await fetch(endpoint, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${authToken}`
                    },
                    body: JSON.stringify(datas),
                });
                const Data = await response.json();
                setData(Data);
            } catch {
                setData(null);            
            }
        // };

    //     fetchPOST();
    // }, []);

    return data;
};

//////////////////////////////////////////////////////////////////////////

export const GETnumero = (ruta, numero) => {
    const extension = ruta;
    const endpoint = `${URL}${extension}${numero}`;
    // console.log(endpoint);
    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchGET = async() => {
            try {
                const authToken = await Token.getToken();
                const response = await fetch(endpoint, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${authToken}`
                    }
                });
                const Data = await response.json();
                // console.log(Data)
                setData(Data);
            } catch {
                setData([]);            
            }
        };
        // console.log(data)
        fetchGET();    
    }, [ruta, numero]);

    return data;
};

export const GETuser = (ruta) => {
    const extension = ruta;
    
    // console.log(endpoint);
    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchGET = async() => {
            try {
                const authToken = await Token.getToken();
                const user = await User.getUser();

                const endpoint = `${URL}${extension}${user}`;
                const response = await fetch(endpoint, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${authToken}`
                    }
                });
                const Data = await response.json();
                // console.log(Data)
                setData(Data);
            } catch {
                setData([]);            
            }
        };
        // console.log(data)
        fetchGET();    
    }, [ruta]);

    return data;
};



export const useGET = (ruta, isFocused) => {
    const extension = ruta;
    const endpoint = `${URL}${extension}`;
    const [data, setData] = useState([]);

    useEffect(() => {
        let intervalId;

        const fetchData = async () => {
            try {
                const authToken = await Token.getToken();
                const response = await fetch(endpoint, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${authToken}`
                    }
                });
                const Data = await response.json();
                setData(Data);
            } catch {
                setData(null);
            }
        };

        const startPolling = () => {
            intervalId = setInterval(fetchData, 500); // Realiza la solicitud cada 5 segundos
        };

        const stopPolling = () => {
            clearInterval(intervalId);
        };

        if (isFocused) {
            startPolling();
        } else {
            stopPolling();
        }

        return stopPolling; // Limpia el intervalo cuando la pestaña no está enfocada
    }, [isFocused]);

    return data;
};


export const POST = async (extension, datos) => {
    const endpoint = `${URL}${extension}`;
    try {
        // console.log(datos);
        const authToken = await Token.getToken();
        const response = await fetch(endpoint, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${authToken}`
                
            },
            body: JSON.stringify(datos),
        });

        if (!response.ok) {
            throw new Error('Error al conectarse');
        }
        // console.log(response.ok);
        return await response.json();
    } catch  {
        console.log('Error en la solicitud POST');/* quitar despues */
        
    }
};

export const PUT = async (numero, ) => {
    try {
      //Conexion del login con el servidor
      const authToken = await Token.getToken();
      const response = await fetch(URL+'', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${authToken}`
        },
        body: JSON.stringify({
          contrasena,
        }),
      });
  
      if (response.ok) {
        console.log('Contraseña actualizada con éxito');
      } else {
        const errorMessage = await response.text();
        throw new Error(`Error al actualizar la contraseña: ${errorMessage}`);
      }
  
    } catch (error) {
      console.error('Error al iniciar sesión:', error.message);
      throw error;
    }
  }
