import { withExpoSnack } from 'nativewind';
import { Platform } from 'react-native';

//Ruta del Login
import {default as LoginScreen}  from "../../Templates/Screens/Login/LoginScreen";
export const Login = OS(LoginScreen)

import {default as ForgotPasswordScreen}  from "../../Templates/Screens/Login/ForgotPassword";
export const ForgotPassword = OS(ForgotPasswordScreen)

import {default as CodigoScreen}  from "../../Templates/Screens/Login/Codigo";
export const Codigo = OS(CodigoScreen)

import {default as PasswodScreen}  from "../../Templates/Screens/Login/Password";
export const Password = OS(PasswodScreen)

//Ruta del Perfil
import {default as PerfilScreen}  from "../../Templates/Screens/Perfil/Perfil";
export const Perfil = OS(PerfilScreen)

//Ruta de Home
import {default as HomeScreen}  from "../../Templates/Screens/Principal/HomeScreen";
export const Home = OS(HomeScreen)

//Ruta de Tools
import {default as ToolsScreen}  from "../../Templates/Screens/Principal/ToolsScreen";
export const Tools = OS(ToolsScreen)

//Ruta de dispositivos
import {default as DevicesScreen}  from "../../Templates/Screens/Principal/DevicesScreen";
export const Devices = OS(DevicesScreen)

//Ruta de Cargando
import {default as LoadingScreen}  from "../../Templates/Screens/Login/LoadingScreen";
export const Loading = OS(LoadingScreen)

import {default as EmpleadosScreen}  from "../../Templates/Screens/Empleados/Empleados";
export const Employees = OS(EmpleadosScreen);
//Empleados por area
import {default as EmpleadosAreaScreen}  from "../../Templates/Screens/Empleados/EmpleadosArea";
export const EmployeesArea = OS(EmpleadosAreaScreen);

import {default as ProduccionScreen}  from "../../Templates/Screens/Produccion/Produccion";
export const Produccion = OS(ProduccionScreen);

import {default as TareasScreen}  from "../../Templates/Screens/Tareas/Tareas";
export const Tareas = OS(TareasScreen);

import {default as AsignarTareas}  from "../../Templates/Screens/Tareas/AsignarTareas";
export const Asignar = OS(AsignarTareas);

import {default as TareasAreaScreen}  from "../../Templates/Screens/Tareas/TareasArea";
export const TareasArea = OS(TareasAreaScreen);

import {default as ProcesosScreen}  from "../../Templates/Screens/Procesos/Procesos";
export const Procesos = OS(ProcesosScreen);

import {default as AlmacenScreen}  from "../../Templates/Screens/Almacen/Almacen";
export const Almacen = OS(AlmacenScreen);

import {default as ModelosScreen}  from "../../Templates/Screens/Almacen/Modelos";
export const Modelos = OS(ModelosScreen);

import {default as DetailModelosScreen}  from "../../Templates/Screens/Almacen/DetailModelos";
export const DetailModelos = OS(DetailModelosScreen);

import {default as PiezasScreen}  from "../../Templates/Screens/Almacen/Piezas";
export const Piezas = OS(PiezasScreen);

import {default as DetailPiezaScreen}  from "../../Templates/Screens/Almacen/DetailPiezas";
export const DetailPieza = OS(DetailPiezaScreen);

import {default as MaterialesScreen}  from "../../Templates/Screens/Almacen/Materiales";
export const Materiales = OS(MaterialesScreen);

import {default as DetailMaterialScreen}  from "../../Templates/Screens/Almacen/DetailMateriales";
export const DetailMaterial = OS(DetailMaterialScreen);

import {default as ReportarScreen}  from "../../Templates/Screens/Tareas/Reportar";
export const Reportar = OS(ReportarScreen);

//---------------------------------------------------------------------------------------




//Validador de dispositivo, si es web transforma las vistas si no se quedan igual v
function OS (screen) {
    return Platform.OS == "web" ? withExpoSnack(screen) : screen
}

