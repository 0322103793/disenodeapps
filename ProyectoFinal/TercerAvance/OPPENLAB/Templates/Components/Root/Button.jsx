import { TouchableOpacity } from "react-native";
import * as Style from "../../Styles/Style";

export const Button = ({children, onPress, style}) => {
    return (
        <TouchableOpacity onPress={onPress} style={[Style.button, style]}>
            {children}
        </TouchableOpacity>
    );
}