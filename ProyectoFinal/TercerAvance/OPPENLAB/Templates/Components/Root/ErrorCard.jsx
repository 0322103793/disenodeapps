import { Card } from "./Card"
import { Icon } from "./Icon"
import { Title } from "./Text"
import { Center } from "./Center"
import * as Style from "../../Styles/Style"
import * as Theme from "../../Styles/Theme"

export const ErrorCard = ({style, mensaje="Problema de conexion", icono="NoWifi"}) => {
    return(
        <Card style={[Style.BigCard, style]}>
            <Center>
                <Icon icon={icono} size={100} style={{tintColor: Theme.color.iconos}}></Icon>
                <Title>{mensaje}</Title>
            </Center>
        </Card>
    )
}