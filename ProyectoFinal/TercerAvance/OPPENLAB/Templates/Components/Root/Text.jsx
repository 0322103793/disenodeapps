import { Text } from "react-native";
import { title, alias, h1,h2,h3, subtitle, infoTitle, subInfo } from "../../Styles/Style.js";

export const Title = ({children, style}) => {
    return(
        <Text style={[title, style]}>{children}</Text>
    )
};

export const SubTitle = ({children, style}) => {
    return(
        <Text style={[subtitle, style]}>{children}</Text>
    )
};

export const Alias = ({children}) => {
    return(
        <Text style={alias}>{children}</Text>
    )
};

export const H2 = ({children, style}) => {
    return(
        <Text style={[h2, style]}>{children}</Text>
    )
};

export const H1 = ({children, style}) => {
    return(
        <Text style={[h1, style]}>{children}</Text>
    )
};

export const H3 = ({children, style}) => {
    return(
        <Text style={[h3, style]}>{children}</Text>
    )
};

export const InfoTitle = ({children, style}) => {
    return(
        <Text style={[infoTitle, style]}>{children}</Text>
    )
};

export const SubInfo = ({children, style}) => {
    return(
        <Text style={[subInfo, style]}>{children}</Text>
    )
};