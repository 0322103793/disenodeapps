import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {StyleSheet, Platform, TouchableOpacity} from 'react-native';
import CustomTabBarButton from './CustomTabBarButtom';
import {useNavigation} from '@react-navigation/native';
import {Devices, Home, Tools, } from '../../../Core/Mains/ScreensMain';

import * as TavNav from '../../Styles/Style';
const Tab = createBottomTabNavigator();

function BottomTabNavigator() {
  const navigation = useNavigation();

  return (
    <Tab.Navigator
      initialRouteName="Home" 
      tabBarPosition="bottom"
      screenOptions={TavNav.screenOptions}>
      <Tab.Screen name="Devices" component={Devices}
        options={{
          tabBarButton: props => <CustomTabBarButton route="Devices" {...props} />
        }} />
      <Tab.Screen name="Home" component={Home} 
        options={{
          tabBarButton: props => <CustomTabBarButton route="Home" {...props} />
        }} />
      <Tab.Screen name="Tools" component={Tools} 
        options={{
            tabBarButton: props => <CustomTabBarButton route="Settings" {...props} />
          }} />
      
    </Tab.Navigator>
  );
}

export default BottomTabNavigator;
