import React, { useState } from 'react';
import Animated, { BounceInUp, BounceInRight, LightSpeedInLeft } from 'react-native-reanimated';
import { Text, TextInput, View, TouchableOpacity, Alert} from 'react-native';

import { Body, Card, Center, Icon, Title, SubTitle } from '../../Components/Mix/Main' 



import { StatusBar } from 'expo-status-bar';
import { LinearGradient } from 'expo-linear-gradient';

import { Login } from '../../../Core/Connection/Login';
import { Screens } from '../../../Core/Mains/NavigatorMain';
import { color, sizes, width } from '../../Styles/Theme';

const LoginScreen = () => {
  const Navigate = Screens();
  const [correo, setcorreo] = useState('');
  const [contrasena, setcontrasena] = useState('');

  async function handleLogin() {
  try {
    const data = await Login(correo, contrasena);
    Alert.alert('Logueado',"Login exitoso");
    Navigate.Tab();
  } catch (error) {
    console.log(error);
    Alert.alert('Error', error.message);
  }
}

  return (
    <View style={{backgroundColor: '#2D3154'}}>
      <View style={{width: '100%', height: '38%'}}>
        <Center>
          <Card style={{width:125, height: 125, backgroundColor: "#FFFFFF", borderRadius: 25}}>
            <Center>
            <Icon icon="Logo" size={125} style={{tintColor: "#2D3154"}}/>
            </Center> 
          </Card>
        </Center>
      </View >
      <View style={{width: '100%', height: '62%', backgroundColor: "#FFFFFF", borderTopRightRadius: 65, borderTopLeftRadius: 10}}>
        <Center>
          <Title style={{color: '#000000', fontSize: 40, marginBottom: 20}}>OPPENLAB</Title>
          <View style={{width: 270, alignSelf: 'auto'}}>

            <SubTitle>correo:</SubTitle> 
            <Animated.View entering={LightSpeedInLeft.delay(350)} className=" bg-black/5 p-3 mt4 rounded-2xl w-full">
              <TextInput placeholder='Correo' value={correo} onChangeText={setcorreo} placeholderTextColor={'gray'}/>
            </Animated.View>


            <SubTitle>contraseña:</SubTitle>
            <Animated.View entering={LightSpeedInLeft.delay(450)} className="w-full">
              <View className="bg-black/5 p-3 rounded-2xl">
                <TextInput placeholder='Contraseña'  value={contrasena} onChangeText={setcontrasena} placeholderTextColor={'gray'}/>
              </View>
            </Animated.View>

            <Animated.View entering={LightSpeedInLeft.delay(500)} className="w-full">
                <TouchableOpacity className=" p-3 w-full bg-[#2D3154] rounded-2xl mt-5 mb-5" onPress={handleLogin}>
                  <Text className="text-xl font-bold text-center text-white">Login</Text>
                </TouchableOpacity>

                <TouchableOpacity className="w-full rounded-full" onPress={Navigate.ForgotPassword} >
                  <Text className="w-full text-xs text-blue-600 text-center ">¿Olvidaste tu contraseña?</Text>
                </TouchableOpacity>
              </Animated.View>

          </View>
        </Center>
      </View>
    </View>
  );
}

export default LoginScreen;


// <Animated.View entering={LightSpeedInLeft.delay(350)} className=" bg-black/5 p-3 mt-3 rounded-2xl w-full">
//                 <TextInput placeholder='Correo' value={correo} onChangeText={setcorreo} placeholderTextColor={'gray'}/>
//               </Animated.View>


{/* <Animated.View entering={LightSpeedInLeft.delay(450)} className="w-full">
                <View className="bg-black/5 p-3 rounded-2xl">
                  <TextInput placeholder='Contrasena'  value={contrasena} onChangeText={setcontrasena} placeholderTextColor={'gray'}/>
                </View>

                <TouchableOpacity className="w-full rounded-full" onPress={Navigate.Forgotcontrasena} >
                  <Text className="w-full text-xs text-blue-600 text-right ">Olvidaste tu contraseña?</Text>
                </TouchableOpacity>
              </Animated.View>

              <Animated.View entering={LightSpeedInLeft.delay(500)} className="w-full">
                <TouchableOpacity className=" p-3 w-full bg-[#4B7583] rounded-full mb-3" onPress={handleLogin}>
                  <Text className="text-xl font-bold text-center text-white">Login</Text>
                </TouchableOpacity>
              </Animated.View> */}