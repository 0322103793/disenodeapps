import { Body, TitleBackCard} from '../../Components/Mix/Main'
import { BigAvatar, Card, Center, Title, H2, H1, SubTitle, Wrap } from '../../Components/Root/Main';
import { ScrollView } from 'react-native';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Style from '../../Styles/Style';


const ProduccionScreen = () => {
    const Navigate = Screens();
    const list = 
        {
            numero: 1,
            url: {uri:"https://www.benavides.com.mx/media/catalog/product/cache/1e14d9bf4283b604380ef95a7c475a07/2/0/20220709_266132.jpg"},
            nombre: "Paracetamol Tabletas",
            descripcion: "Tabletas de Paracetamol de 500mg, 10 tabletas",
            stock: 1000,
        };

    return ( 
        <Body>
            <TitleBackCard onPress={ Navigate.Back }>Producción</TitleBackCard>
            <ScrollView>
            <Wrap>
            <Card style={[Style.GrandCard, {height: 250}]}>
                    <Center>
                        <BigAvatar url={list.url}></BigAvatar>
                    </Center>
                </Card>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: 300, flex: 1 }]}>
                <Title style={{marginBottom:25}}>Detalles</Title>
                <Title style={{marginBottom:25,alignSelf: "flex-start"}} >Nombre: {list.nombre}</Title>
                <Title style={{marginBottom:25,alignSelf: "flex-start"}} >Descripción: {list.descripcion}</Title>
                <Title style={{marginBottom:25,alignSelf: "flex-start"}} >Stock: {list.stock}</Title>
            </Card>
            </Wrap>
            </ScrollView>
        </Body>
    )
}

export default ProduccionScreen;