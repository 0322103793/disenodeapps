import { Body, TitleBackCard, H1, Center, Title, Card, SubTitle, SubInfo, InfoUserArea, Button,} from '../../Components/Mix/Main'
import {TextInput, Alert, } from 'react-native';
import {CardContent, InfoTitle,} from '../../Components/Root/Main';
import React, { useState } from 'react';
import { Screens } from "../../../Core/Mains/NavigatorMain";
import * as Theme from '../../Styles/Theme';
import * as Style from '../../Styles/Style';
import { getUser as usuario } from '../../../Core/Data/User';
import { GETuser, POST } from '../../../Core/Connection/Global';

function pressButton(descripcion, estado, user, area) {
    try {
        POST('tareas', {
            "descripcion": descripcion,
            "estado": estado,
            "empleado": user,
            "area": area,
        });

        Alert.alert('Tarea asignada');
    } catch (error) {
        console.log(error);
        Alert.alert('Error', error.message);
    }
}

const ReportarScreen = ({route}) => {
    const { tarea } =  route.params;
    const user = usuario();
    const area = GETuser('userArea/');
    const Navigate = Screens();
    
    const [descripcion, setdescripcion] = useState('');

    return (
        <Body>
            <TitleBackCard onPress={Navigate.Back}>Hacer Reporte</TitleBackCard>
            <Card style={[Style.TopCard, {justifyContent: 'space-between',height: "60%"}]}>
            <Title style={{marginTop: "8%"}}>¿Ha ocurrido algún evento durante los labores que requiera ser documentado? Realice un reporte:</Title>
            <SubTitle style={{color: "white"}}>Descripción:</SubTitle>
                <Card style={{backgroundColor: "#FFFFFF", borderRadius: 25, width: "95%"}}>
                    <TextInput style={{marginHorizontal: 5}} placeholder='Descripción del Reporte.' value={descripcion} onChangeText={setdescripcion} placeholderTextColor={'gray'} />
                </Card>
                <Button style={{ marginTop: 5 }} onPress={pressButton}>
                    <H1>Entregar</H1>
                </Button>
                </Card>
                <InfoUserArea list={area}/>

                <Card style={[Style.InfoCard, Style.InfoCard.horizontal]}>
                    <CardContent>
                        <InfoTitle>Tarea número {tarea ? tarea.numero : ''}</InfoTitle> 
                        <SubInfo>{tarea ? tarea.descripcion : ''}</SubInfo>
                    </CardContent>
                </Card>
        </Body>
    )
}


export default ReportarScreen;
