import * as Theme from './Theme';


//----------------------------------------------------------------
//                  CONTENEDORES

export const horizontal = {
    flexDirection: 'row',
    alignItems: 'center',
    
}
export const viewFlex = {
    flex: 1,
    backgroundColor: Theme.color.fondo,
    padding: Theme.spacing.m,
    
}
export const container = {
    width: '100%',
    height: '100%',    
}
export const center = {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
    
}

export const wraper = {
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'space-between'
}

export const content = {
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingHorizontal: Theme.spacing.m,
    paddingVertical: Theme.spacing.l / 2,
    paddingRight: Theme.spacing.m / 2
}

//----------------------------------------------------------------


//----------------------------------------------------------------
//                      TEXTOS

export const alias = {
    color: Theme.color.texto.estatico,
}
export const h1 = {
    color: Theme.color.texto.estatico,
    
}
export const h2 = {
    color: Theme.color.texto.normal,
    fontSize: 11
}
export const title = {
    color: Theme.color.texto.estatico,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    fontSize: Theme.sizes.h3,
    fontWeight: 'bold',
    marginVertical: "0%",
    alignSelf: 'center',    
    
}
export const h3 = {
    color: Theme.color.texto.estatico,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    // fontFamily: 'Abel-Regular',
    fontSize: Theme.sizes.title,
    fontWeight: 'bold',
    marginVertical: "5%",
    alignSelf: 'center',
      
}

export const infoTitle = {
    marginVertical: 4,
    fontSize: Theme.sizes.body,
    fontWeight: 'bold',
    color: Theme.color.texto.estatico,
}

export const subInfo = {
    fontSize: 12,
    color: Theme.color.texto.light,
}

export const subtitle = {
    color: Theme.color.texto.normal,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    fontSize: Theme.sizes.h3,

    marginTop: "6%",
    marginBottom: "3%",
    marginLeft: Theme.spacing.s
      
    
}
//----------------------------------------------------------------



//----------------------------------------------------------------
//                      BOTONES

export const button = {
    borderRadius: 100,
    width:  140,
    height: 30,
    backgroundColor: Theme.color.tertiary,
    alignItems: 'center',
    justifyContent: 'center',
    
}

//----------------------------------------------------------------



//----------------------------------------------------------------
//                      IMAGENES

export const avatar = {
    borderRadius: 100,
    overflow: 'hidden',
    backgroundColor: Theme.color.fourth,
    width: 80,
    height: 80,
    borderWidth: 2, // Ancho del borde
    borderColor: Theme.color.bordes,
}

export const bigAvatar = {
    borderRadius: 25,
    overflow: 'hidden',
    backgroundColor: Theme.color.fourth,
    width: 250,
    height: 170,
    borderWidth: 2, // Ancho del borde
    borderColor: Theme.color.bordes.color1,
    marginBottom:20,
    
}

export const image = {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
}

export const media = {
    flex: 1,
    borderTopLeftRadius: Theme.sizes.radius,
    borderTopRightRadius: Theme.sizes.radius,
    backgroundColor: Theme.color.fourth,
    overflow: 'hidden',

    borderBottomRadius: {
        borderBottomLeftRadius: Theme.sizes.radius,
        borderBottomRightRadius: Theme.sizes.radius,
    },
}

//-----------------------------------------------------



//-------------------------------------------------------
//                      CARTAS

export const card = {
    width: 200, 
    height: 200,
    borderWidth: Theme.color.bordes.ancho1,
    borderColor: Theme.color.bordes.color2,
    elevation: 5,
    margin: 2,
    
}

export const buttonCard = {
    backgroundColor: Theme.color.primary,
    borderRadius: 20, 
    paddingHorizontal: 1, 
    paddingVertical: 1,  
    width: 100,
    height:100  
}

export const TopCard = {
    width: "100%",
    marginBottom: "2%",
    backgroundColor: Theme.color.secundary, 
    borderRadius: Theme.sizes.radius,
    paddingHorizontal: "6%"
}

export const GrandCard = {
    width: Theme.width -40,
    height: 400,
    marginBottom: "2%",
    backgroundColor: Theme.color.tertiary, 
    borderRadius: Theme.sizes.radius,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
}

export const PresentationCard = {
    width: Theme.width -40,
    height: 400,
    marginBottom: "2%",
    borderRadius: Theme.sizes.radius,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    elevation: 0,
    margin: 0,
    borderColor: "transparent",
}

export const AcordionCard = {
    justifyContent: 'space-between',
    backgroundColor: Theme.color.primary,
    borderRadius: Theme.sizes.radius,
    borderWidth: Theme.color.bordes.ancho1,
    borderColor: Theme.color.bordes.color2,
    marginTop: "2%",
    marginBottom: '5%',
    width: '100%',
    height: 100,
    elevation: 5,

    SubCard: {
        width: Theme.width -40,
        
        flex: 1,
        elevation: 5,
        
        

        borderWidth: Theme.color.bordes.ancho2,
        borderColor: Theme.color.bordes.color2,
        borderTopWidth: 0,

       
        backgroundColor: Theme.color.secundary,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        borderRadius: Theme.sizes.radius,
        paddingHorizontal: "1%",
        marginHorizontal: 2,
        }
}

export const BigCard = {
    width: "100%",
    marginBottom: "2%",
    backgroundColor: Theme.color.tertiary, 
    borderRadius: Theme.sizes.radius,
    paddingHorizontal: "6%"
}

export const SubCard = {
    width: "100%",
    marginBottom: "5%",
    backgroundColor: Theme.color.tertiary, 
    borderRadius: Theme.sizes.radius,
    paddingHorizontal: "6%",
    borderWidth: Theme.color.bordes.ancho2,
    borderColor: Theme.color.bordes.color2,
    height: "50%", 
    marginTop: 15
}

export const Device = {
    paddingTop: 10,
    width: Theme.width-40,
    marginBottom: "5%",
    backgroundColor: Theme.color.secundary, 
    borderRadius: Theme.sizes.radius,
    paddingHorizontal: "6%",
    flexDirection: 'row',
    justifyContent:'space-between',
    alignItems: 'center',
    
    control: {
        height: Theme.height/5*3-5
    },
    vista: {
        height: Theme.height/20*5-5,
    }
}

export const TitleCard = {
    width: "100%",
    marginBottom: "5%",
    backgroundColor: Theme.color.primary, 
    borderRadius: Theme.sizes.radius,
    paddingHorizontal: "0%",
    flexDirection: 'row',
    justifyContent:'center ',
    alignItems: 'center',
    height: '10%'
}

export const InfoCard = {
    width: 150,
    height: 220,
    justifyContent: 'space-between',
    marginBottom: Theme.spacing.l,
    backgroundColor: Theme.color.tertiary,
    borderRadius: Theme.sizes.radius,
    borderWidth: Theme.color.bordes.ancho2,
    borderColor: Theme.color.bordes.color2,

    horizontal: {
        width: Theme.width - 40,
        height: 100,
    }
}

//-------------------------------------------------




//-------------------------------------------------
//              CARROUSELES  
export const CarrouselCard = {
    paddingButtom: Theme.spacing.l,   
    
    static : {
        width: Theme.sizes.width-40,
        height: 300,
        withAndSpace: Theme.sizes.width + Theme.spacing.l,       
    }
}

export const CarouselList = {
    paddingVertical: Theme.spacing.l,    
    
    static : {
        
        marginRight: Theme.spacing.s+2,
        marginLeft: Theme.spacing.s+2,
        width: 80,
        height: 80,
        withAndSpace: Theme.sizes.width + Theme.spacing.l
    }
}

export const CarouselInfo = {
    paddingVertical: Theme.spacing.s,    
    
    static : {
        
        // marginRight: Theme.spacing.s+2,
        // marginLeft: Theme.spacing.s+2,
        width: Theme.sizes.width-40,
        height: 150,
        withAndSpace: Theme.sizes.width + Theme.spacing.l
        
    }
}

//-------------------------------------------------------


//-------------------------------------------------------
//              BARRA DE NAVEGACION

export const screenOptions={
    headerShown: false,
    tabBarInactiveTintColor: "transparent",
    tabBarActiveTintColor: "transparent",
    tabBarShowLabel: true,
    tabBarShowIcon: true,
    tabBarIndicator: false,
    tabBarIndicatorStyle: { 
        backgroundColor: Theme.color.nav.indicador, 
        paddingTop: Theme.spacing.s,
    },
    tabBarStyle: { 
        // backgroundColor: Theme.color.nav.fondo, 
        // height: '8%', 
        // width: Theme.sizes.full,
        // position: 'absolute',
    // backgroundColor: 'transparent',
        marginTop: 10,
        borderTopWidth: 0,
        borderRadius: 25,
        bottom: 15,
        right: 10,
        left: 10,
        height: '5%',
        width: Theme.width-20,
        
    },
}

export const Icon = {
    focusTrue: Theme.color.nav.iconFocus,
    focusFalse: Theme.color.nav.icon,
    size: 35,
}

//----------------------------------------------------------------